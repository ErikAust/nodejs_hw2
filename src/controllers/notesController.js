const express = require('express');
const router = new express.Router();

const {
    getUsersNotes,
    addNoteForUser,
    getUsersNoteById,
    updateUsersNoteById,
    checkUsersNoteById,
    deleteUsersNoteById
} = require('../services/notesService');

const {
    asyncWrapper
} = require('../utils/apiUtils');

router.get('/', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const { offset, limit } = req.query;

    const notes = await getUsersNotes(userId, offset, limit);

    res.status(200).json({ notes });
}));

router.post('/', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const data = req.body;

    await addNoteForUser(userId, data);

    res.status(200).json({ message: 'Success' });
}));

router.get('/:id', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
     const id = req.params.id;

    const note = await getUsersNoteById(id, userId);

    res.status(200).json({ note });
}));

router.put('/:id', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
     const id = req.params.id;
    const data = req.body;

    await updateUsersNoteById(id, userId, data);

    res.status(200).json({ message: 'Success' });
}));

router.patch('/:id', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
     const id = req.params.id;

    await checkUsersNoteById(id, userId);
    res.status(200).json({ message: 'Success' });
}));

router.delete('/:id', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const id = req.params.id;

    await deleteUsersNoteById(id, userId);
    res.status(200).json({ message: 'Success' });
}));

module.exports = {
    notesRouter: router
};
