const express = require('express');
const router = new express.Router();

const {
    register,
    login
} = require('../services/authService');

const {
    asyncWrapper
} = require('../utils/apiUtils');

router.post('/register', asyncWrapper(async (req, res) => {
    const {
        username,
        password
    } = req.body;

    await register({ username, password });

    res.status(200).json({ message: "Success" });
}));

router.post('/login', asyncWrapper(async (req, res) => {
    const {
        username,
        password
    } = req.body;

    const token = await login({ username, password });

    res.status(200).json({
        message: "Success",
        jwt_token: token
    });
}));

module.exports = {
    authRouter: router
}
