const bcrypt = require('bcrypt');

const { User } = require('../models/userModel');

const infoUser = async (_id) => {
    const user = await User.findOne({ _id }, '-__v -password');

    if (!user) {
        throw new Error('No user found');
    }
    return user;
}

const deleteUser = async (_id) => {
    const user = await User.findOneAndDelete({ _id });

    if (!user) {
        throw new Error('No user found');
    }
    return user;
};

const changeUsersPassword = async (_id, oldPassword, newPassword) => {
    const user = await User.findOne({ _id });

    if (!user) {
        throw new Error('No user found');
    }

    if (!(await bcrypt.compare(oldPassword, user.password))) {
        throw new Error('Wrong password!');
    }

    await User.updateOne({ _id }, {
        $set: {
            password: await bcrypt.hash(newPassword, 10)
        }
    });
};

module.exports = {
    infoUser,
    deleteUser,
    changeUsersPassword
}