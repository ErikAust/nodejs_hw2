const { Note } = require('../models/noteModel');

const getUsersNotes = async (userId, offset = 0, limit = 0) => {
    const count = await Note.find({ userId }).countDocuments();
    const notes = await Note.find({ userId }, '-__v')
        .skip(+offset)
        .limit(+limit);

    if (!notes) {
        throw new Error('No notes whit such id found')
    }

    return {
        offset,
        limit,
        count: count,
        notes: notes
    };
};

const addNoteForUser = async (userId, notePayLoad) => {
    const note = new Note({ ...notePayLoad, userId });
    await note.save();
};

const getUsersNoteById = async (noteId, userId) => {
    const note = await Note.findOne({ _id: noteId, userId }, '-__v');

    if (!note) {
        throw new Error('No note with such id found!');
    }

    return note;
};

const updateUsersNoteById = async (noteId, userId, data) => {
    const note = await Note.findOne({ _id: noteId, userId });

    if (!note) {
        throw new Error('No note with such id found!');
    }

    await Note.findOneAndUpdate({ _id: noteId, userId }, { $set: data });
};

const checkUsersNoteById = async (noteId, userId) => {
    const note = await Note.findOne({ _id: noteId, userId });

    if (!note) {
        throw new Error('No note with such id found!');
    }

    await note.updateOne({
        $set: {
            completed: !note.completed
        }
    });
};

const deleteUsersNoteById = async (noteId, userId) => {
    const note = await Note.findOne({ _id: noteId, userId });

    if (!note) {
        throw new Error('No note with such id found!');
    }

    await Note.findOneAndRemove({ _id: noteId, userId });
};

module.exports = {
    getUsersNotes,
    addNoteForUser,
    getUsersNoteById,
    updateUsersNoteById,
    checkUsersNoteById,
    deleteUsersNoteById
};