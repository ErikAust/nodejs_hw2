const express = require('express');
const app = express();
const morgan = require('morgan');
const mongoose = require('mongoose');
require('dotenv').config();
const PORT = process.env.PORT || 8080;

const { notesRouter } = require('./src/controllers/notesController');
const { usersRouter } = require('./src/controllers/usersController');
const { authRouter } = require('./src/controllers/authController');
const { authMiddleware } = require('./src/middlewares/authMiddleware');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);

app.use(authMiddleware);
app.use('/api/users/me', usersRouter);
app.use('/api/notes', notesRouter);

app.use((req, res, next) => {
    res.status(404).json({ message: 'Not found' });
});
app.use((err, req, res, next) => {
    res.status(500).json({ message: err.message });
});

const start = async () => {
    try {
        await mongoose.connect('mongodb+srv://user1:qwery@cluster0.xvraa.mongodb.net/myFirstDatabase?retryWrites=true&w=majority', {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
            useFindAndModify: false
        });
        app.listen(PORT);
    } catch (err) {
        console.error(err.message);
    }
};

start();